<?php

declare(strict_types=1);

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\RefreshDatabaseTrait;

/**
 * @internal
 * @coversNothing
 */
class UserTest extends ApiTestCase
{
    // This trait provided by HautelookAliceBundle will take care of refreshing the database
    // content to a known state before each test
    use RefreshDatabaseTrait;
    use AuthenticationTestTrait;

    public function testConnection(): void
    {
        $client = static::createClient();
        $client->request('POST', '/authentication_token', ['json' => [
            'email' => 'admin@mail.fr',
            'password' => 'password',
        ]]);

        $this->assertResponseStatusCodeSame(200);
        $this->assertResponseHeaderSame('content-type', 'application/json');
    }

    public function testRequestWithAuthorization(): void
    {
        $client = static::createAdminClient();
        $client->request('GET', '/');

        $this->assertResponseStatusCodeSame(200);
    }

    public function testRequestWithoutAuthorization(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseStatusCodeSame(401);
    }
}
