<?php

declare(strict_types=1);

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\Client;

trait AuthenticationTestTrait
{
    /**
     * Create a client with a default Authorization header.
     *
     * @param string $email
     * @param string $password
     */
    protected static function createAuthenticatedClient($email = 'user@mail.fr', $password = 'password'): Client
    {
        $data = [
            'email' => $email,
            'password' => $password,
        ];

        $response = static::createClient()->request('POST', '/authentication_token', ['json' => $data]);

        $responseData = $response->toArray();

        $client = static::createClient();
        $client->setDefaultOptions(['auth_bearer' => $responseData['token']]);

        return $client;
    }

    /**
     * Create a client with a admin Authorization header.
     */
    protected static function createAdminClient(): Client
    {
        $email = 'admin@mail.fr';
        $password = 'password';

        return self::createAuthenticatedClient($email, $password);
    }
}
