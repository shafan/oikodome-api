# API Bootstrapped

[![CI](https://gitlab.com/shafan/api-bootstrapped/badges/master/pipeline.svg)](https://gitlab.com/shafan/api-bootstrapped/commits/master)

## Install

    make          # self documented makefile
    make install  # install and start the project
